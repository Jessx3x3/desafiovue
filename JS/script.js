document.addEventListener('DOMContentLoaded', function () {
    new Vue({
        el: '#content',
        data: {
            nombre: '',
            descripcion: '',
            precio: '',
            formActualizar: false,
            idActualizar: -1,
            nombreActualizar: '',
            descripcionActualizar: '',
            precioActualizar: '',
            productos: [] 
        },
        methods: {
            crearProducto: function () {
                if(this.nombre != '' && this.precio != ''){
                    this.productos.push({
                        id: Math.round(Math.random() * 100),
                        nombre: this.nombre,
                        descripcion: this.descripcion,
                        precio: this.precio
                    });
                }else{
                    alert('Rellenar Campos obligatorios');
                }
                this.nombre = '';
                this.descripcion = '';
                this.precio = '';
            },
            verFormActualizar: function (producto_id) {
                this.idActualizar = producto_id;
                this.nombreActualizar = this.productos[producto_id].nombre;
                this.descripcionActualizar = this.productos[producto_id].descripcion;
                this.precioActualizar = this.productos[producto_id].precio;
                this.formActualizar = true;
            },
            borrarProducto: function (producto_id) {
                this.productos.splice(producto_id, 1);
            },
            guardarActualizacion: function (producto_id) {
                this.formActualizar = false;
                this.productos[producto_id].nombre = this.nombreActualizar;
                this.productos[producto_id].descripcion = this.descripcionActualizar;
                this.productos[producto_id].edad = this.edadActualizar;
            }
        }
    });
});